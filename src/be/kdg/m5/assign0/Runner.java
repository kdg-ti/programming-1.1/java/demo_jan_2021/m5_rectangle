package be.kdg.m5.assign0;

/**
 * Author: derijkej
 */
public class Runner {
	public static void main(String[] args) {

		Rectangle box = new Rectangle(12,23,34,45);
		System.out.println("I'm located at position (" +
			box.getX() + ","+box.getY()+")");
		System.out.println("My size is " + box.surface());
		box.setWidth(10);
		System.out.println("My size is now " + box.surface());
		box.setWidth(1.9);
		Rectangle otherBox = new Rectangle();
		System.out.println("otherBox at position (" +
			otherBox.getX() + ","+otherBox.getY()+")");
		System.out.println("TO STRING TEST");
	//	System.out.println("Empty Rectangle: " + otherBox.toString());
	System.out.println("Empty Rectangle: " + otherBox);
		System.out.println(box.toString());
	}
}
